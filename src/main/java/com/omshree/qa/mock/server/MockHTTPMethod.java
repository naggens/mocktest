package com.omshree.qa.mock.server;

public enum MockHTTPMethod {
	
	SOAP("POST"), GET("GET"), POST("POST");
	
	private String httpMethod;

	MockHTTPMethod(String httpMethod){
		this.httpMethod = httpMethod;
	}
	
	public String httpMethod(){
		return httpMethod;
	}
}
