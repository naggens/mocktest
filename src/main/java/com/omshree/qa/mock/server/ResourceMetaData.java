package com.omshree.qa.mock.server;

public class ResourceMetaData {
	private MockHTTPMethod method;
	private String resourceUrl;
	private String mediaType;
	private String response;
	private boolean requiredFiltering;
	private String mockServiceName;
	private String soapAction;
	private String responseFilePath;
	private String serviceRequestContainsString;
	
	public MockHTTPMethod getMethod() {
		return method;
	}
	public void setMethod(MockHTTPMethod method) {
		this.method = method;
	}
	public String getResourceUrl() {
		return resourceUrl;
	}
	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}
	public String getMediaType() {
		return mediaType;
	}
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public boolean isRequiredFiltering() {
		return requiredFiltering;
	}
	public void setRequiredFiltering(boolean requiredFiltering) {
		this.requiredFiltering = requiredFiltering;
	}
	public String getMockServiceName() {
		return mockServiceName;
	}
	public void setMockServiceName(String mockServiceName) {
		this.mockServiceName = mockServiceName;
	}
	public String getSoapAction() {
		return soapAction;
	}
	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}
	public String getResponseFilePath() {
		return responseFilePath;
	}
	public void setResponseFilePath(String responseFilePath) {
		this.responseFilePath = responseFilePath;
	}
	public String getServiceRequestContainsString() {
		return serviceRequestContainsString;
	}
	public void setServiceRequestContainsString(String serviceRequestContainsString) {
		this.serviceRequestContainsString = serviceRequestContainsString;
	}
}
