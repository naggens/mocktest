package com.omshree.qa.mock.server;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;

public class ResourceUtil {
	public Set<Resource> rebuildResources(){
		Set<Resource> resourceSet = new HashSet<>();
		final Resource.Builder resourceBuilder = Resource.builder();
        resourceBuilder.path("helloworld");
 
        final ResourceMethod.Builder methodBuilder = resourceBuilder.addMethod("GET");
        methodBuilder.produces(MediaType.TEXT_PLAIN_TYPE)
                .handledBy(new Inflector<ContainerRequestContext, String>() {
 
            @Override
            public String apply(ContainerRequestContext containerRequestContext) {
                return "Hello World!";
            }
        });
 
        resourceSet.add(resourceBuilder.build());
        return resourceSet;
	}
}
