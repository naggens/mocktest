package com.omshree.qa.mock.server.jetty;

public interface MockServer {
	//First method to be called for setting up environment
	public void setupServer();
	//Method called to start server
	public void startServer(int portNumber);
	//method called to stop server
	public void stopServer();
	
}
