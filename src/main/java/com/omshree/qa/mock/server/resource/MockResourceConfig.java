package com.omshree.qa.mock.server.resource;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.container.ContainerRequestContext;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;

import com.omshree.qa.mock.server.MockHTTPMethod;
import com.omshree.qa.mock.server.ResourceMetaData;
import com.omshree.qa.mock.server.util.ResourceMetaDataLoader;

public class MockResourceConfig extends ResourceConfig{
	
	public MockResourceConfig() throws Exception{
		init();
	}
	private void init() throws Exception{
		Set<Resource> resourceSet = new HashSet<>();
		List<ResourceMetaData> resourceMetaDataList = new ResourceMetaDataLoader().populateMetaDataFromDB();
		if(CollectionUtils.isNotEmpty(resourceMetaDataList)){
			CollectionUtils.filter(resourceMetaDataList, new Predicate<ResourceMetaData>() {

				@Override
				public boolean evaluate(ResourceMetaData object) {
					return !StringUtils.equalsIgnoreCase(object.getMethod().name(), MockHTTPMethod.SOAP.name());
				}
				
			});
			for(final ResourceMetaData metaData: resourceMetaDataList){
				final Resource.Builder resourceBuilder = Resource.builder();
		        //resourceBuilder.path("helloworld");
				resourceBuilder.path(metaData.getResourceUrl());
		 
		        //final ResourceMethod.Builder methodBuilder = resourceBuilder.addMethod("GET");
				final ResourceMethod.Builder methodBuilder = resourceBuilder.addMethod(metaData.getMethod().httpMethod());
		        //methodBuilder.produces(MediaType.TEXT_PLAIN_TYPE)
				methodBuilder.produces(metaData.getMediaType())
		                .handledBy(new Inflector<ContainerRequestContext, String>() {
		 
		            @Override
		            public String apply(ContainerRequestContext containerRequestContext) {
		                return metaData.getResponse();
		            }
		        });
		 
		        resourceSet.add(resourceBuilder.build());
			}
		}
		registerResources(resourceSet);
	}
}
