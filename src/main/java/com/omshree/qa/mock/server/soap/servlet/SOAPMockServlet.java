package com.omshree.qa.mock.server.soap.servlet;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omshree.qa.mock.server.MockHTTPMethod;
import com.omshree.qa.mock.server.ResourceMetaData;
import com.omshree.qa.mock.server.util.MockServerConstants;
import com.omshree.qa.mock.server.util.ResourceMetaDataLoader;

@SuppressWarnings("serial")
public class SOAPMockServlet extends HttpServlet{
	
	private static Logger log = LoggerFactory.getLogger(SOAPMockServlet.class);
	private HashMap<String, ResourceMetaData> soapActionUriMap = new HashMap<>();
	private HashMap<String, ResourceMetaData> soapRequestUriMap = new HashMap<>();
	private ResourceMetaDataLoader loader;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().print("<html><body><h2>GET operation is not supported</h2></body></html>");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/**
		 * SOAP 1.1
		 * Content-Type: text/xml
		 * SOAPAction: "http://example.com/ticker"
		 * 
		 * SOAP 1.2
		 * Content-Type: application/soap+_xml; action=http://example.com/ticker
		 * 
		 */
		String uri = request.getPathInfo();
		System.out.println("URI is: " + uri);
		String soapAction = request.getHeader(MockServerConstants.SOAP_ACTION_1_1);
		soapAction = StringUtils.remove(soapAction, "\"");
		if(StringUtils.isEmpty(soapAction)){
			String contentType = request.getContentType();
			if(StringUtils.isNotEmpty(contentType)){
				String split[] = StringUtils.split(contentType, ";");
				if((split != null) && (split.length > 1)){
					if(StringUtils.isNotEmpty(split[1])){
						String[] content = StringUtils.trim(split[1]).split("=");
						if((content != null) && (content.length > 1)){
							if(StringUtils.equalsIgnoreCase(StringUtils.trim(content[0]), MockServerConstants.SOAP_ACTION_1_2)){
								soapAction = StringUtils.trim(content[1]);
							}
						}else{
							//return fault message
						}
					}else{
						//return fault message
					}
				}else{
					//return fault message
				}
			}else{
				//return fault message
			}
		}
		
		System.out.println("soapAction value is: " + soapAction);
		
		String requestXML = IOUtils.toString(request.getInputStream(), Charset.defaultCharset());
		System.out.println("Request XML: " + requestXML);
		
		if(StringUtils.isEmpty(soapAction)){
			for(String key: soapRequestUriMap.keySet()){
				ResourceMetaData data = soapRequestUriMap.get(key);
				if(StringUtils.contains(requestXML, data.getServiceRequestContainsString())){
					response.getOutputStream().write(data.getResponse().getBytes());
					return;
				}
			}
		}else{
			ResourceMetaData data = soapActionUriMap.get(soapAction + "|||" + uri);
			response.getOutputStream().write(data.getResponse().getBytes());
		}
		//super.doPost(request, response);
	}

	@Override
	public void init() throws ServletException {
		try {
			loader = new ResourceMetaDataLoader();
			List<ResourceMetaData> dataList = loader.populateMetaDataFromDB();
			if(CollectionUtils.isNotEmpty(dataList)){
				CollectionUtils.filter(dataList, new Predicate<ResourceMetaData>() {

					@Override
					public boolean evaluate(ResourceMetaData object) {
						return StringUtils.equalsIgnoreCase(object.getMethod().name(), MockHTTPMethod.SOAP.name());
					}
					
				});
				
				for(ResourceMetaData data: dataList){
					soapActionUriMap.put((data.getSoapAction() + "|||" + data.getResourceUrl()), data);
					soapRequestUriMap.put((data.getServiceRequestContainsString() + "|||" + data.getResourceUrl()) , data);
				}
			}
			
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		
		
		super.init();
	}

}
