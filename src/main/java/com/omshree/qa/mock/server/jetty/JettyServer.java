package com.omshree.qa.mock.server.jetty;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JettyServer {
	private static Logger log = LoggerFactory.getLogger(JettyServer.class);
	private Server server;
	// Port number
	private int portNumber;

	// Host name
	private String hostName = "localhost";
	// idleTimeout in ms
	private int idleTimeout = 30000;

	public void startServer() throws IOException {
		if (server == null) {
			setupJettyEnvironment();
			initializeJettyServer();
		} else if (server.isRunning()) {
			log.info("Jetty server is already running at port number : " + this.portNumber);
		} else if (server.isStarting() || server.isStarted()) {
			log.info("server is already started at port number : " + this.portNumber);
		}
	}

	private void setupJettyEnvironment() throws IOException {
		String propertiesFile = System.getProperty("mock.environment.properties");
		FileInputStream inputStream = new FileInputStream(propertiesFile);
		Properties prop = new Properties();
		prop.load(inputStream);
		portNumber = Integer.parseInt(prop.getProperty("jetty.port", "8080"));
		hostName = prop.getProperty("jetty.host.name", "localhost");
		idleTimeout = Integer.parseInt(prop.getProperty("jetty.idle.timeout", "30000"));
	}

	private void initializeJettyServer() {
		server = new Server(portNumber);
		// HTTP connector
		ServerConnector http = new ServerConnector(server);
		http.setHost(hostName);
		http.setPort(portNumber);
		http.setIdleTimeout(idleTimeout);

		server.addConnector(http);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);

		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class,
				"/REST/*");
		jerseyServlet.setInitOrder(1);
		jerseyServlet.setInitParameter("javax.ws.rs.Application",
				"com.omshree.qa.mock.server.resource.MockResourceConfig");
		jerseyServlet.setInitParameter("resourceBase", "src/main/webapp");
		jerseyServlet.setInitParameter("pathInfoOnly", "true");

		ServletHolder soapServlet = context.addServlet(com.omshree.qa.mock.server.soap.servlet.SOAPMockServlet.class,
				"/SOAP/*");
		soapServlet.setInitOrder(2);

		// ServletHolder staticServlet =
		// context.addServlet(DefaultServlet.class,"/*");
		// staticServlet.setInitParameter("resourceBase","src/main/webapp");
		// staticServlet.setInitParameter("pathInfoOnly","true");

		try {
			server.start();
			// server.join();
		} catch (Throwable ex) {
			log.error("Error while Starting Jetty Server: ", ex);
		}

		log.info("Jetty server is started Successfully at port number : " + this.portNumber);
	}

	public void stopServer() {
		try {
			server.stop();
			log.info("Jetty server is stopped Successfully at port number : " + this.portNumber);
		} catch (Throwable t) {
			t.printStackTrace(System.err);
		}
	}
}
