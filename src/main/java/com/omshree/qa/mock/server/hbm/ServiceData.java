package com.omshree.qa.mock.server.hbm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ServiceData")
public class ServiceData {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	//srv_name
	@Column(name = "srv_name")
	private String serviceName;
	//srv_uri
	@Column(name = "srv_uri")
	private String serviceUri;
	//srv_type
	@Column(name = "srv_type")
	private String serviceType;
	//srv_http_method
	@Column(name = "srv_http_method")
	private String serviceHttpMethod;
	//srv_content_type
	@Column(name = "srv_content_type")
	private String serviceContentType;
	//srv_soap_action
	@Column(name = "srv_soap_action")
	private String serviceSoapAction;
	//file or String
	//srv_resp_type
	@Column(name = "srv_resp_type")
	private String serviceResponseType;
	//srv_resp_path
	@Column(name = "srv_resp_path")
	private String serviceResponsePath;
	//srv_resp_str
	@Column(name = "srv_resp_str")
	private String serviceResponseString;
	//srv_req_contains_str
	@Column(name = "srv_req_contains_str")
	private String serviceRequestContainsString;
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceUri() {
		return serviceUri;
	}
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getServiceHttpMethod() {
		return serviceHttpMethod;
	}
	public void setServiceHttpMethod(String serviceHttpMethod) {
		this.serviceHttpMethod = serviceHttpMethod;
	}
	public String getServiceContentType() {
		return serviceContentType;
	}
	public void setServiceContentType(String serviceContentType) {
		this.serviceContentType = serviceContentType;
	}
	public String getServiceSoapAction() {
		return serviceSoapAction;
	}
	public void setServiceSoapAction(String serviceSoapAction) {
		this.serviceSoapAction = serviceSoapAction;
	}
	public String getServiceResponseType() {
		return serviceResponseType;
	}
	public void setServiceResponseType(String serviceResponseType) {
		this.serviceResponseType = serviceResponseType;
	}
	public String getServiceResponsePath() {
		return serviceResponsePath;
	}
	public void setServiceResponsePath(String serviceResponsePath) {
		this.serviceResponsePath = serviceResponsePath;
	}
	public String getServiceResponseString() {
		return serviceResponseString;
	}
	public void setServiceResponseString(String serviceResponseString) {
		this.serviceResponseString = serviceResponseString;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getServiceRequestContainsString() {
		return serviceRequestContainsString;
	}
	public void setServiceRequestContainsString(String serviceRequestContainsString) {
		this.serviceRequestContainsString = serviceRequestContainsString;
	}
}
