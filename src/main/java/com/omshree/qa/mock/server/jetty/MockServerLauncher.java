package com.omshree.qa.mock.server.jetty;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omshree.qa.mock.server.db.HsqlDBServer;
import com.omshree.qa.mock.server.util.HibernateUtil;

public class MockServerLauncher {
	private static Logger log = LoggerFactory.getLogger(MockServerLauncher.class);
	public static void main(String[] args) throws Exception{
		initializeLogger();
		log.info("Preparing to start mock server....");
		final JettyServer server = new JettyServer();
		final HsqlDBServer dbServer = new HsqlDBServer();
		dbServer.startDBServer();
		server.startServer();
		//HibernateUtil.populateData();
		log.info("Mock Server Started Successfully !!!");
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run() {
				log.info("Preparing to stop mock server....");
				try{
					server.stopServer();
				}catch (Throwable ex){
					log.error("Error while stopping jetty server ", ex);
				}
				try{
					HibernateUtil.shutdown();
					dbServer.stopDBServer();
				}catch (Throwable ex){
					log.error("Error while stopping db server ", ex);
				}
				log.info("Mock Server stopped Successfully !!!");
			}
		});
	}
	
	public static void initializeLogger() throws Exception {
		String propertiesFile = System.getProperty("mock.environment.properties");
		FileInputStream inputStream = new FileInputStream(propertiesFile);
		Properties prop = new Properties();
		prop.load(inputStream);
		PropertyConfigurator.configure(prop);
		log.info("Log file initialized.....");
	}
}
