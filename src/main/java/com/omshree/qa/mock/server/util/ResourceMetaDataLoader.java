package com.omshree.qa.mock.server.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.omshree.qa.mock.server.MockHTTPMethod;
import com.omshree.qa.mock.server.ResourceMetaData;
import com.omshree.qa.mock.server.ResponseType;
import com.omshree.qa.mock.server.hbm.ServiceData;

public class ResourceMetaDataLoader {
	private String propertyFileName = "/resource.config.properties";
	private String configurationFileName = "resource.config.properties";

	public List<ResourceMetaData> populateMetaDataFromDB() throws IOException {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		List<ResourceMetaData> resourceMetaDataList = new ArrayList<>();
		Session session = null;
		try {
			session = factory.openSession();
			Transaction tx = session.beginTransaction();
			List<ServiceData> dataList = session.createCriteria(ServiceData.class).list();
			tx.commit();

			if (CollectionUtils.isNotEmpty(dataList)) {
				for (ServiceData data : dataList) {
					ResourceMetaData metaData = new ResourceMetaData();
					metaData.setMediaType(data.getServiceContentType());
					metaData.setResourceUrl(data.getServiceUri());
					metaData.setResponse(data.getServiceResponseString());
					String method = data.getServiceHttpMethod();
					metaData.setSoapAction(data.getServiceSoapAction());
					metaData.setServiceRequestContainsString(data.getServiceRequestContainsString());

					String respType = data.getServiceResponseType();
					if (StringUtils.equalsIgnoreCase(respType, ResponseType.file.name())) {
						String filePath = data.getServiceResponsePath();
						metaData.setResponseFilePath(filePath);
						metaData.setResponse(readFileContent(filePath));
					}

					for (MockHTTPMethod mockHttpMethod : MockHTTPMethod.values()) {
						if (StringUtils.equalsIgnoreCase(method, mockHttpMethod.name())) {
							metaData.setMethod(mockHttpMethod);
							break;
						}
					}
					resourceMetaDataList.add(metaData);
				}
			}
		} finally {
			if (session != null)
				session.close();
		}
		return resourceMetaDataList;
	}

	public List<ResourceMetaData> populateMetaData() throws IOException {
		List<ResourceMetaData> resourceMetaDataList = new ArrayList<>();
		Properties prop = new Properties();
		prop.load(ResourceMetaDataLoader.class.getResourceAsStream(propertyFileName));
		ResourceMetaData metaData = new ResourceMetaData();
		metaData.setMediaType(prop.getProperty("mock.service.mediatype"));
		metaData.setResourceUrl(prop.getProperty("mock.service.url"));
		metaData.setResponse(prop.getProperty("mock.service.response"));
		String method = prop.getProperty("mock.service.method");
		metaData.setSoapAction(prop.getProperty("mock.service.method.action"));

		String respType = prop.getProperty("mock.service.response.type");
		if (StringUtils.equalsIgnoreCase(respType, ResponseType.file.name())) {
			String filePath = prop.getProperty("mock.service.response.file.path");
			metaData.setResponseFilePath(filePath);
			metaData.setResponse(readFileContent(filePath));
		}

		for (MockHTTPMethod mockHttpMethod : MockHTTPMethod.values()) {
			if (StringUtils.equalsIgnoreCase(method, mockHttpMethod.name())) {
				metaData.setMethod(mockHttpMethod);
				break;
			}
		}
		resourceMetaDataList.add(metaData);
		return resourceMetaDataList;
	}

	public List<ResourceMetaData> populateMetaDataFromConfiguration() throws Exception {
		FileBasedConfigurationBuilder<PropertiesConfiguration> builder = new FileBasedConfigurationBuilder<PropertiesConfiguration>(
				PropertiesConfiguration.class)
						.configure(new Parameters().properties().setFileName(configurationFileName)
								.setThrowExceptionOnMissing(true)
								.setListDelimiterHandler(new DefaultListDelimiterHandler(';'))
								.setIncludesAllowed(false));
		PropertiesConfiguration config = builder.getConfiguration();

		List<ResourceMetaData> resourceMetaDataList = new ArrayList<>();
		ResourceMetaData metaData = new ResourceMetaData();
		metaData.setMediaType(config.getString("mock.service.mediatype"));
		metaData.setResourceUrl(config.getString("mock.service.url"));
		metaData.setResponse(config.getString("mock.service.response"));
		String method = config.getString("mock.service.method");
		metaData.setSoapAction(config.getString("mock.service.method.action"));

		String respType = config.getString("mock.service.response.type");
		if (StringUtils.equalsIgnoreCase(respType, ResponseType.file.name())) {
			String filePath = config.getString("mock.service.response.file.path");
			metaData.setResponseFilePath(filePath);
			metaData.setResponse(readFileContent(filePath));
		}

		for (MockHTTPMethod mockHttpMethod : MockHTTPMethod.values()) {
			if (StringUtils.equalsIgnoreCase(method, mockHttpMethod.name())) {
				metaData.setMethod(mockHttpMethod);
				break;
			}
		}
		resourceMetaDataList.add(metaData);
		return resourceMetaDataList;
	}

	private String readFileContent(String file) throws FileNotFoundException, IOException {
		return IOUtils.toString(new FileInputStream(new File(file)), Charset.defaultCharset());
	}
}
