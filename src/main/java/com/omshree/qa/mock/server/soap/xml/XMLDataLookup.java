package com.omshree.qa.mock.server.soap.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.collections4.CollectionUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class XMLDataLookup {
	public void lookupData(InputStream instream, List<String> xpaths) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		HashMap<String, String> returnMap = new HashMap<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(instream);
		//Element rootElement = document.getDocumentElement();
		XPath xPath = XPathFactory.newInstance().newXPath();
		
		if(CollectionUtils.isNotEmpty(xpaths)){
			for(String xpath: xpaths){
	            Node node = (Node) xPath.evaluate(xpath, document, XPathConstants.NODE);
	            returnMap.put(xpath, node.getNodeValue());
			}
		}
	}
}
