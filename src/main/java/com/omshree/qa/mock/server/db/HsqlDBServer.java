package com.omshree.qa.mock.server.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.hsqldb.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HsqlDBServer {
	
	private static Logger log = LoggerFactory.getLogger(HsqlDBServer.class);
	private int portNumber;
	private String hostName;
	private String dbFilePath;
	private String dbName;
	private Server dbServer;
	
	private void setupDBEnvironment() throws IOException {
		String propertiesFile = System.getProperty("mock.environment.properties");
		FileInputStream inputStream = new FileInputStream(propertiesFile);
		Properties prop = new Properties();
		prop.load(inputStream);
		portNumber = Integer.parseInt(prop.getProperty("hsql.db.port", "1234"));
		hostName = prop.getProperty("hsql.db.host.name", "localhost");
		dbFilePath = prop.getProperty("hsql.db.file.path");
		dbName = prop.getProperty("hsql.db.name");
	}
	
	public void startDBServer() throws IOException{
		setupDBEnvironment();
		dbServer = new Server();
		dbServer.setAddress(hostName);
		dbServer.setDatabaseName(0, dbName);
		dbServer.setDatabasePath(0, dbFilePath);
		dbServer.setPort(portNumber);
		dbServer.setTrace(true);
		dbServer.setRestartOnShutdown(false);
		dbServer.setNoSystemExit(true);
		dbServer.start();
		log.info("HSQL db server started at port number {}", portNumber);
	}
	
	public void stopDBServer(){
		if(dbServer != null){
			dbServer.stop();
			log.info("HSQL db server stopped at port number {}", portNumber);
		}
	}
}
