package com.omshree.qa.mock.server.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.omshree.qa.mock.server.hbm.ServiceData;

public class HibernateUtil {
	private static SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			if (sessionFactory == null) {
				Configuration configuration = new Configuration()
						.configure(HibernateUtil.class.getResource("/hibernate.cfg.xml"));
				configuration.addPackage("com.omshree.qa.mock.server.hbm");
				configuration.addAnnotatedClass(com.omshree.qa.mock.server.hbm.ServiceData.class);
				StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
				serviceRegistryBuilder.applySettings(configuration.getProperties());
				ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			}
			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}
	
	public static void populateData(){
		Session session = sessionFactory.openSession();
		try{
			Transaction tx = session.beginTransaction();
			ServiceData serviceData = new ServiceData();
			//serviceData.setId(1);
			serviceData.setServiceName("SoapMock");
			serviceData.setServiceUri("/CreateOrderWebService/CreateOrder-v1.0");
			serviceData.setServiceContentType("application/xml");
			serviceData.setServiceType("SOAP");
			serviceData.setServiceSoapAction("http://www.mst.macys.com/xsd/order/prepareOrder");
			serviceData.setServiceContentType("file");
			serviceData.setServiceResponsePath("/Users/m083526/dev/monitoring/qa.mock.server/src/main/resources/prepare_order_response.xml");
			session.saveOrUpdate(serviceData);
			tx.commit();
		}finally{
			session.close();
		}
	}
}
